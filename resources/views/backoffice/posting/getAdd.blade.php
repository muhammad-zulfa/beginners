@extends('template.backoffice')

@section('content')
        <ul class="breadcrumb breadcrumb-page">
			<li><a href="<?php echo url('backoffice')?>">Beranda</a></li>
			<li><a href="<?php echo url('backoffice/postings')?>">Grup Posting</a></li>
			<li class="active"><a href="#">Tambah Data</a></li>
		</ul>
		<div class="page-header">
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-user page-header-icon"></i>&nbsp;&nbsp;<?php echo $pageTitle?></h1>
			</div>
		</div> <!-- / .page-header -->

        <div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Tambah Data</span>
					</div>
					<div class="panel-body">
						<form action="<?php echo url('backoffice/postings/submit'); ?>" class="form-horizontal" id="" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
							<div class="form-group">
								<label for="name" class="col-sm-3 control-label">Judul</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required" id="name" name="judul" maxlength="100" placeholder="Nama" />
								</div>
							</div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Label</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control required" id="label" name="label" maxlength="100" placeholder="Nama" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="group" class="col-sm-3 control-label">Group Post</label>
                                <div class="col-sm-9">
                                <select name="group" class="form-control required" required="yes" id="group">
                                    <option selected>Chose Group</option>
                                    @foreach($kategori as $row)
                                        <option value="{{$row->group_id}}">{{$row->group_name}}</option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="post" class="col-sm-3 control-label">Post</label>
                                <div class="col-sm-9">
                                <textarea name="post" id="editor1" rows="10" cols="80" placeholder="Post here">
                                </textarea>
                                </div>
                            </div>

                            <div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" name="save" value="save" class="btn btn-primary">Simpan</button>
									<a class="btn btn-default" href="<?php echo url('backoffice/postings')?>">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
        <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'editor1' );
        </script>

@endsection

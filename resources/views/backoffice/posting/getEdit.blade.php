@extends('template.backoffice')

@section('content')
        <ul class="breadcrumb breadcrumb-page">
			<li><a href="<?php echo url('backoffice')?>">Beranda</a></li>
			<li><a href="<?php echo url('backoffice/posting/post-category')?>">Grup Posting</a></li>
			<li class="active"><a href="#">Ubah Data</a></li>
		</ul>
		<div class="page-header">
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-user page-header-icon"></i>&nbsp;&nbsp;<?php echo $pageTitle?></h1>
			</div>
		</div> <!-- / .page-header -->

        <div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Ubah Data</span>
					</div>
					<div class="panel-body">
						<form action="<?php echo url('backoffice/posting/post-category/submit'); ?>" class="form-horizontal" id="" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="group_id" value="{{ $obj->group_id }}">
							<div class="form-group">
								<label for="name" class="col-sm-3 control-label">Nama</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required" id="name" name="name" maxlength="100" placeholder="Nama" value="{{ $obj->group_name }}" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" name="save" value="save" class="btn btn-primary">Simpan</button>
									<a class="btn btn-default" href="<?php echo url('backoffice/posting/post-category')?>">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection

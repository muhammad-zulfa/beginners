@extends('template.backoffice')

@section('content')
        <ul class="breadcrumb breadcrumb-page">
			<li><a href="<?php echo url('backoffice')?>">Beranda</a></li>
			<li><a href="#">Administrasi</a></li>
			<li><a href="<?php echo url('backoffice/administration/users')?>">Pengguna</a></li>
			<li class="active"><a href="#">Tambah Pengguna</a></li>
		</ul>
		<div class="page-header">
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-user page-header-icon"></i>&nbsp;&nbsp;<?php echo $pageTitle?></h1>
			</div>
		</div> <!-- / .page-header -->

        <div class="row">
			<div class="col-sm-12">
				<!-- Javascript -->
				<script>
					init.push(function () {
						// Setup validation
						$("#form-validate").validate({
							focusInvalid: false,
							rules: {
								'username': {
									required: true,
						            remote: "<?php echo url('backoffice/administration/users/check-username')?>"
								},
								'email': {
									  required: true,
									  email: true,
									  remote: "<?php echo url('backoffice/administration/users/check-email')?>"
								},
								'password': {
									required: true,
									minlength: 8,
									maxlength: 20
								},
								'password-confirm': {
									required: true,
									minlength: 8,
									equalTo: "#password"
								},
								'group': {
									required: true
								}
							},
							messages: {
						    	'username': {
									remote: $.validator.format("Username '{0}' is already in use")
								},
								'email': {
									remote: $.validator.format("Email '{0}' is already in use")
								}
							},
						});
					});
				</script>
				<!-- / Javascript -->

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Tambah Pengguna</span>
					</div>
					<div class="panel-body">
						<form action="<?php echo url('backoffice/administration/users/submit'); ?>" class="form-horizontal" id="form-validate" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							
							<div class="form-group">
								<label for="fullname" class="col-sm-3 control-label">Nama Lengkap</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required" id="fullname" name="fullname" maxlength="100" placeholder="Nama lengkap">
								</div>
							</div>
							<div class="form-group">
								<label for="username" class="col-sm-3 control-label">Username</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="username" name="username" maxlength="100" placeholder="Username">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" id="email" name="email" maxlength="100" placeholder="Email">
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password" name="password" placeholder="Password" maxlength="60">
								</div>
							</div>

							<div class="form-group">
								<label for="password-confirm" class="col-sm-3 control-label">Konfirmasi password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password-confirm" name="password-confirm" placeholder="Konfirmasi password">
									<p class="help-block">Ketik ulang password anda sesuai dengan yang anda masukan sebelumnya.</p>
								</div>
							</div>

							<div class="form-group">
								<label for="phone" class="col-sm-3 control-label">Telp</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required" id="phone" name="phone" maxlength="20" placeholder="Phone: 0812 345 67890">
								</div>
							</div>

							<div class="form-group">
								<label for="bio" class="col-sm-3 control-label">Bio</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="bio" id="bio" maxlength="500"></textarea>
									<p class="help-block">Deskripsi profile singkat.</p>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" name="save" value="save" class="btn btn-primary">Tambah</button>
									<a class="btn btn-default" href="<?php echo url('backoffice/administration/users')?>">Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection

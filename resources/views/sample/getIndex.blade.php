@extends('template.backoffice')

@section('content')
        <ul class="breadcrumb breadcrumb-page">
			<li><a href="<?php echo url('backoffice')?>">Beranda</a></li>
			<li class="active"><a href="#">Sample</a></li>
		</ul>
		<div class="page-header">
			
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-user page-header-icon"></i>&nbsp;&nbsp;<?php echo $pageTitle?></h1>

				<div class="col-xs-12 col-sm-8">
					<div class="row">
						<hr class="visible-xs no-grid-gutter-h">
						<!-- "Create project" button, width=auto on desktops -->
						<div class="pull-right col-xs-12 col-sm-auto"><a href="<?php echo url('sample/add')?>" class="btn btn-primary btn-labeled" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span>Tambah Data</a></div>

						<!-- Margin -->
						<div class="visible-xs clearfix form-group-margin"></div>

						<!-- Search field -->
						<form action="" class="pull-right col-xs-12 col-sm-6">
							<div class="input-group no-margin">
								<span class="input-group-addon" style="border:none;background: #fff;background: rgba(0,0,0,.05);"><i class="fa fa-search"></i></span>
								<input type="text" placeholder="Cari..." class="form-control no-padding-hr" style="border:none;background: #fff;background: rgba(0,0,0,.05);">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div> <!-- / .page-header -->

        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Daftar Sample</span>
					</div>
					<div class="panel-body table-responsive">
						<table class="table table-striped table-hover table-bordered" id="jq-datatable">
							<thead>
								<tr>
									<th style="width: 30px">No.</th>
									<th style="width: 200px;">Nama</th>
									<th>Keterangan</th>
									<th style="width: 60px;">Aksi</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<script>
    		init.push(function () {
    			$('#jq-datatable').dataTable( {
    		        "processing": true,
    		        "serverSide": true,
    		        "paginationType": "full_numbers",
    		        "ajax": "<?php echo url('sample/ajax-list')?>",
    		        "language": {
    		            "processing": "<div style='padding-top: 6px;'>Mohon Tunggu.. Sedang memproses data..</div>"
    		        },
    		        "columns": [
                        {"searchable": false, className: "text-center", orderable: false},
                        null,
                        null,
                        {"searchable": false, className: "text-center", orderable: false}
    		    	]
    		    } );
    		    
    			$('#jq-datatable_wrapper .dataTables_filter input').attr('placeholder', 'Pencarian...');
    		});
		</script>
@endsection

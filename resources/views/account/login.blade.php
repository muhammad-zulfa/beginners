@extends('template.login')

@section('content')
    <div class="login-title">Login</div> 
		<div class="content-login">
		    @if (count($errors) > 0)
			<div class="alert alert-danger">
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				    @foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			
		    <form class="form-signin" id="validate" role="form" method="POST" action="<?php echo url('account/login')?>">
			    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group">
					<input type="text" class="form-control required" name="login" value="{{ old('email') }}" />
				</div>
				<div class="form-group">
				   <input type="password" class="form-control required" name="password" />
				</div>
			    <div class="checkbox">
				    <label>
				      <input type="checkbox" name="remember" /> Ingat saya
				    </label>
				    <span class="pull-right">
				    	<a href="<?php echo url('password/email')?>">Lupa Password?</a>
				    </span>
				</div>
				<button class="btn btn-md btn-primary btn-block" type="submit" style="font-weight: bold;">
					<i class="fa fa-sign-in" style="margin-right: 10px;"></i> LOGIN
				</button>		
			</form>
		</div>
@endsection

@section('jsscript')
    <script type="text/javascript">
        $(document).ready(function (){
        	
        });
    </script>
@endsection

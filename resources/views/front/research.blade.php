@extends('template.front.front')
@section('content')
  <section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Research</h2>
                <p class="lead">Kegitan-kegiatan pelatihan</p>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 Kesehatan Karang di Pulau Pari Lab. Biodiversitas dan Biosistimatika Kelautan IPB</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 Bioekologi dan Genetika Populasi Hiu Paus di Teluk Cendrawasih. Sumber WWF-Indonesia dan Lab. Biodiversitas dan Biosistimatika Kelautan IPB</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 Dinamika larva ikan di Lagoon Pulau Pari, Sumber Dana Lab. Biodiversitas dan Biosistimatika Kelautan IPB</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 Ikan Hiu DNA Barcoding di Pelabuhan Ratu, Sumber Dana IBRC Bali/Lab. Biodiversitas dan Biosistimatika Kelautan IPB</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 Penyakit karang di pulau pulau kecil di Indonesia, Sumber Lab. Biodiversitas dan Biosistimatika Kelautan IPB/Subdit Inventarisasi dan Identifikasi Pulau, Kementerian Kelautan Perikanan</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2012 Biodiversitas krustase DNA  Barcoding di terumbu dari jenis Pocillopora dan Seriatopora, Sumber Dana IBRC Bali/Lab. Biodiversitas dan Biosistimatika Kelautan IPB</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2012 Survei Mamalia Laut di Teluk Cendrawasih, Repsol</h3>
                        </div>
                    </div>
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2012 Biodiversitas sumber daya laut dan pulau – pulau kecil di teluk Banten (Pulau Tunda, Pulau Pamujan Besar, Pulau Pamujan Kecil dan Pulau Panjang), Sumber Dana Pemda Kabupaten Serang/CENTRAS IPB</h3>
                        </div>
                    </div><div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>2011 Atraktor Planula Karang di Kepulauan Seribu, Sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
                    </div>
                </div>
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>2011 Studi tentang penyakit-penyakit karang di Pulau Bawean Kabupaten Gresik, sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
                    </div>
                </div>

                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>2011 Studi tentang Kesehatan  karang Famili Fingidea di Pulau Sepanjang, Pagerungan dan Kangean, Kabupaten Sumenep, Madura. sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
                    </div>
                </div><div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="feature-wrap">

                    <!--<i class="fa fa-bullhorn"></i>-->
                    <i class="fa fa-leaf"></i>
                    <h3>2010 – 2011 Transplantasi Spons Laut pada sistem tertutup (kolam), sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
                </div>
            </div>
                <div class="feature-wrap">

                    <!--<i class="fa fa-bullhorn"></i>-->
                    <i class="fa fa-leaf"></i>
                    <h3>2010 Transplantasi Karang lunak pada sistem tertutup (kolam), sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
                </div>
            </div>
            <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2010 Studi tentang penyakit-penyakit karang di Pulau Mansuar Raja Ampat Papua, sumber dana Lab. Hidrobiologi Laut Departemen ITK-IPB (Peneliti)</h3>
            </div>
        </div>
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2009 Studi tentang penyakit-penyakit karang di Kepulauan Seribu, sumber dana Pribadi (Peneliti)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="feature-wrap">

            <!--<i class="fa fa-bullhorn"></i>-->
            <i class="fa fa-leaf"></i>
            <h3>2009 Biodiversitas biota di Selat Makassar sumber dana DIKTI (Peneliti)</h3>
        </div>
    </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2008 Inventarisasi Karang di Pulau Air, Pulau Karang Beras, Pulau Panggan dan Pulau Pramuka, Kepualuan Seribu, Sumber dana Pemda DKI,</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2008 Survei kondisi terumbu karang di Pulau Pagerungan, Sepanjang, Kangean Barat dan Karang Takat, Sumenep, Madura, Sumber dana Kangean Energi Indonesia</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2008-2009 Teknologi Fragmentasi Buatan Karang Lunak (Octocoralia: Alcyonacea) Sebagai Penghasil Bioaktif Untuk Penyediaan Bahan Obat-Obatan, sumber dana KNRT (Peneliti)</h3>
            </div>
        </div>

        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2007-2008 Kajian Bioaktif Karang Lunak (Octocoralia: Alcyonacea) Gorgonian sp, Lobophytum sp, Hasil Fragmentasi Buatan sebagai Penyedia Bahan Obat-Obatan dari Laut, sumber dana DIKTI-Hibah Bersaing (Peneliti)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2007 Survei kondisi terumbu karang calon lokasi terumbu buatan, Lombok Barat, DKP.</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2007 Survei kondisi terumbu karang calon lokasi terumbu buatan Biorock, Tanjung Lesung</h3>
            </div>
        </div>

        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2006 Distribution of Epifauna on Floating seaweed in Japan Coastal Waters, Sponsor Shimoda Marine Research Center, University of Tsukuba, Japan (Enumerator)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2006 Fatty acid composition of Incisocaliope symbioticus (Amphipod: Gammaridea) and Melithea flabellifera (Octocoralia: Gorgonian) Sponsored by Shimoda Marine Research Center, University of Tsukuba, Japan (Peneliti)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2005-2007 Teknik Reproduksi Spons Laut Kelas Demospongiae Untuk Sediaan Senyawa Bioaktif dari Laut, sumber dana DIKTI-Hibah Penelitian Tim Pascasarjana (Peneliti)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2005-2007 Studi Teknologi Mineral akresi sebagai terumbu buatan yang artistic dan ramah lingkungan, sumber dana KNRT (Enumerator)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2004 Pengembangan Terumbu Buatan dengan Metode Biorock, sponsor Pribadi (Peneliti)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2003 Pengembangan Coral Garden di Pulau Pari Kepulauan Seribu dengan menggunakan teknik transplantasi , sumber dana DAAD (Peneliti).</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2002-2004 Rekayasa Teknologi Fragmentasi Buatan Pada Karang Masif Jenis Langka (Cynarina, Caulastrea, Plerogyra, Blastomusa) dalam Upaya Rehabilitasi dan Peningkatan Produksi, sumber dana KNRT, (Enumerator)</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>2000 Pemetaan kondisi ekosistem terumbu karang di beberapa pulau (Pulau Kotok and Gosong Laga) di Kepulauan Seribu, sumber dana Taman Nasional Kepulauan Seribu (Enumerator)</h3>
            </div>
        </div>
        </div><!--/.services-->
            </div><!--/.row-->



        </div><!--/.container-->
    </section><!--/#feature-->


@endsection
@section('script')
 <script src="{{asset('template/front/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#lima").addClass('active');
 </script>
@endsection
@extends('template.front.front')
@section('content')

    <section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Community Empowerement</h2>
                <p class="lead"></p>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2012 November, Coral Identification Training using Coral Finder Toolkits at Tulamben Bali</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>2013 March , Coral Identification Training using Coral Finder Toolkits at Wakatobi, Sulawesi Tenggara</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Pelatihan Transplantasi Karang Hias dan Sponge, Dinas Perikanan Kelautan dan Pertanian Kota Bontang, Kalimantan Timur 3-5 Mei 2012</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Pelatihan Sertifikasi Monitoring Terumbu Karang, Departemen Ilmu dan Teknologi Kelautan, melalui Laboratorium Selam Ilmiah (Scientific Diving Laboratory) Bagian Hidrobiologi Laut bekerja sama dengan Dinas Kelautan, Perikanan, Enenrgi dan Sumberdaya Mineral (DKPESDM) Kabupaten Serang tanggal 19 – 22 November 2013 di Pulau Tunda Kabupaten Serang Banten</h3>
                        </div>
                    </div><!--/.col-md-4-->



        </div><!--/.services-->
            </div><!--/.row-->



        </div><!--/.container-->
    </section><!--/#feature-->



@endsection

@section('script')
 <script src="{{asset('template/front/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#enam").addClass('active');
 </script>
@endsection
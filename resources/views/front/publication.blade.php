@extends('template.front.front')
@section('content')

    <section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Publication</h2>
                <p class="lead"></p>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Subhan B, HM Madduppa, D Arafat, MR Himawan, HC Ramadhana, RA Pasaribu, A Bramandito, D Khairudi, MI Panggarbesi. (2020). Kehidupan Laut Tropis Tulamben, IPBPress, 138+X</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Himawan M. R., Tania C., Noor B. A., Wijonarno A., Subhan B., Madduppa H., 2020 Sex and size range composition of whale shark (Rhincodon typus) and their sighting behaviour in relation with fishermen lift-net within Cenderawasih Bay National Park, Indonesia. AACL Bioflux 8(2):123-133.</br>
                            </h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                                            <div class="feature-wrap">

                                                <!--<i class="fa fa-bullhorn"></i>-->
                                                <i class="fa fa-leaf"></i>
                                <h3>Prehadi, Adrianus Sembiring, Eka Maya Kurniasih, Rahmad, Dondy Arafat, Beginer Subhan, Hawis Madduppa .2020. DNA barcoding and phylogenetic reconstruction of shark species landed in Muncar fisheries landing site in comparison with Southern Java fishing port. BIODIVERSITAS  Volume 16, Number 1, April 2020 Pages: 55-61</h3>
                                            </div>
                                        </div>

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Munawaroh MA, A Wardian, D Arafat, B Subhan. 2014. Keragaan Pulau Kecil di Kabupaten Serang, Provinsi Banten. IPB Press. Bogor, 38+xiv hal</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Beginer Subhan, Hawis Madduppa, Dondy Arafat, Dedi Soedharma (2014) Bisakah Transplantasi Karang Perbaiki Ekosistem Terumbu Karang. Jurnal Risalah Kebijakan Pertanian dan Lingkungan  Vol 2 no 3 Desember 2014:209-164</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Madduppa HH, Zamani NP, Subhan B, Aktani U, Ferse  SCA (2014) Feeding behavior and diet of the eight-banded butterflyfish Chaetodon octofasciatus in the Thousand Islands, Indonesia. Environmental Biology of Fishes DOI 10.1007/s10641-014-0225</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Zamani NP, K Abdallah, B Subhan, 2013 Electrical Current Stimulates Coral Branching and Growth in Jakarta Bay in Thomas J. Goreau and Robert Kent (eds) Trench Innovative Methods of Marine Ecosystem Restoration.CRC Press. 81-88</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Madduppa HH, Subhan B, Suparyani E, Siregar AM, Arafat D, Tarigan SA, Alimuddin, Khairudi D, Rahmawati F, Bramandito A. 2013.
                                Dynamics of fish diversity across an environmental gradient in the Seribu Islands reefs off Jakarta. Biodiversitas 14: 17-24.</h3>
                        </div>
                    </div>
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Madduppa HH, Subhan B, Suparyani E, Siregar AM, Arafat D, Tarigan SA, Alimuddin, Khairudi D, Rahmawati F, Bramandito A. 2013.
                                Dynamics of fish diversity across an environmental gradient in the Seribu Islands reefs off Jakarta. Biodiversitas 14: 17-24.</h3>
                        </div>
                    </div><div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>Beginer Subhan, Dedi Soedharma, Dondy Arafat, Hawis Madduppa, Fadillah Rahmawati, Ayu Ervinia, Aditya Bramandito, Denny Khaerudi, Ahmad Taufik GhozaliPengaruh Cahaya Terhadap Tingkat Kelangsungan Hidup Dan Pertumbuhan Karang Lunak Lobophytum Strictum (Octocoralia: Alcyonacea). Hasil Transplantasi Pada Sistem Resirkulasi. The Effect Of Light On Survival And Growth Rate Of Transplanted Soft Coral Lobophytum Strictum (Octocoralia: Alcyonacea) In Recirculation System.Jurnal Teknologi Perikanan dan Kelautan. Vol. 3. No. 1 November 2012: 37-44</h3>
                    </div>
                </div>
                    <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>HAWIS H. MADDUPPA,SYAMSUL B. AGUS,AULIA R. FARHAN,DEDE SUHENDRA, BEGINER SUBHAN. 2012. Fish biodiversity in coral reefs and lagoon at the Maratua Island, East Kalimantan: Biodiversitas Vol: 13 Issue: 3 : 145-200</h3>
                    </div>
                </div>

                    <div class="feature-wrap">

                        <!--<i class="fa fa-bullhorn"></i>-->
                        <i class="fa fa-leaf"></i>
                        <h3>Subhan B, F Rahmawati, D Arafat, NA Bayu. 2011. Kondisi Kesehatan Karang Fungiidae di Perairan Pulau Pramuka, Kepulauan Seribu. Jurnal Teknologi Perikanan dan Kelautan 2 (1): 41-50</h3>
                    </div>
                </div><div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="feature-wrap">

                    <!--<i class="fa fa-bullhorn"></i>-->
                    <i class="fa fa-leaf"></i>
                    <h3>Azis AM, MM. Kamal, NP. Zamani, B.Subhan. 2011. Coral Settelment on Concrate Artificial Reef in Pramuka Island waters, Kepulauan Seribu, Jakarta and Management Option. Jurnal of Indonesian Coral Reefs Volume 1, number 1:55-64</h3>
                </div>
            </div>
                <div class="feature-wrap">

                    <!--<i class="fa fa-bullhorn"></i>-->
                    <i class="fa fa-leaf"></i>
                    <h3>B.Subhan, D.Arafat, F. Rahmawati, ML. Hakim, D.Soedharma. 2011. Identifikasi Penyakit Karang di Pulau Pramuka, Kepulauan Seribu. (eds) DG. Bengen, A.Sunuddin, CSU.Dewi. Prosiding Simposium Nasional Pengelolaan Pesisir, Laut, dan Pulau-pulau Kecil. Himpunan Ahli Pengelolaan Pesisir Indonesia (HAPPI). Jakarta.</h3></div>
            </div>
            <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Zamani NP, R.Bachtiar, HM. Madduppa, JW.Adi, J.Isnul, M.Iqbal, B.Subhan.2010. Study on Biorock Technique Using Three Different Anode Materials (Magnesium, Alumunium, and Titanium). E-jurnal Ilmu dan Teknologi Kelautan Tropis, Vol.2, No.1, Hal 1-8</h3>
            </div>
        </div>
            <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>B. Subhan D Soedharma, NP Zamani, M Kawaroe, MA Setiawan . 2010. Survival Rate and Growth of Transplanted sponge Aaptos Aaptos in Pari Island, Seribu Islands, Jakarta Proceeding of Coral Reef Management Symposium on Coral Triangle Area. (eds) Jamaludin Jompa, Riyanto Basuki, Suraji, Mike Teroso, Eva Tri Lestari. COREMAP II, Directorate General of Marine, Coasts and Small Island. Ministry of Marine Affairs and Fisheries : 68 – 78</h3></div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="feature-wrap">

            <!--<i class="fa fa-bullhorn"></i>-->
            <i class="fa fa-leaf"></i>
            <h3>Arafat D., NP Zamani, A Winarto, D Soedharma, M Kawaroe, H Effendi, B. Subhan. 2010. Soft Coral Growth (Octocorallia : Alcyonacea) Lobophytum strictum and Sinularia dura As a Result Fragmentation in Pramuka Island, Seribu Island, Jakarta. Proceeding of Coral Reef Management Symposium on Coral Triangle Area. (eds) Jamaludin Jompa, Riyanto Basuki, Suraji, Mike Teroso, Eva Tri Lestari. COREMAP II, Directorate General of Marine, Coasts and Small Island. Ministry of Marine Affairs and Fisheries : 101 – 109</h3>
        </div>
    </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Aoki M., K. Tanaka, N. Kumagai, A. Ito, B. Subhan, T. Komatsu.2009.Patterns of Organization in Ephiphytic Animal Communities on Floating Seaweed. Bulletin on Coastal Oceanography. Vol. 46 no.2:137-140 [Japanese version, published]</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Zamani NP, B.Subhan, HH. Madduppa, R.Bachtiar, M.Destianto, T.Maulina, 2009. Pengaruh Biorock terhadap Keragaman dan Kelimpahan Ikan Karang di Tanjung Lesung, Banten. Prosiding Simposium Nasional Terumbu Karang II. Program Rehabilitasi dan Pengelolaan Terumbu Karang CORMAP II. Direktorat Jenderal Kelautan dan Pulau-pulau Kecil Departemen Kelautan dan Perikanan.</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Subhan B,  D Soedharma, H Madduppa, D. Arafat, D. Heptarina 2008. Kelangsungan Hidup dan Pertumbuhan Karang jenis Euphyllia sp, Plerogyra sinuosa dan Cynarina lacrymalis yang ditransplantasikan di Pulau Pari, Kepulauan Seribu, Jakarta.  Prosiding Seminar Nasional Penelitian-penelitian Kelautan dan Perikanan, Universitas Brawijaya.</h3>
            </div>
        </div>

        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Subhan B, D Arafat, G Andono, Mursalin, H Madduppa.2008. Studi Penutupan karang di Pulau Karang Beras, Pulau Air, and Pulau Panggang, Seribu Islands, Jakarta . Prosiding Seminar Nasional Penelitian-penelitian Kelautan dan Perikanan, Universitas Brawijaya. [Indonesia version, published]</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Madduppa HM, B Subhan, R Bachtiar, MS Ismet, Y Budikartini, MD Bria.2007. Prospek Terumbu Buatan Biorock dalam Peningkatan Sumberdaya Ikan di Kepulauan Seribu. Prosiding Munas Terumbu Karang 2007, Program Rehabilitasi Terumbu Karang COREMAP II, Departemen Kelautan dan Perikanan.</h3>
            </div>
        </div>
        <div class="col-md-15 col-sm-15 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="feature-wrap">

                <!--<i class="fa fa-bullhorn"></i>-->
                <i class="fa fa-leaf"></i>
                <h3>Soedharma D, B Subhan.2007. Transplantasi Karang Saat Ini dan Tantanganya di Masa Depan. Prosiding Munas Terumbu Karang 2007, Program Rehabilitasi Terumbu Karang COREMAP II, Departemen Kelautan dan Perikanan.</h3>
            </div>
        </div>

        </div><!--/.services-->
            </div><!--/.row-->



        </div><!--/.container-->
    </section><!--/#feature-->
@endsection

@section('script')
 <script src="{{asset('template/front/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#tujuh").addClass('active');
 </script>
@endsection
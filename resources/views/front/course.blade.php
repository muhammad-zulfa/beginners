@extends('template.front.front')
@section('content')
<section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Courses</h2>
                <p class="lead"></p>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="{{asset('template/home/images/ico/ikan2.png')}}" width="100px" height="100px">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Biologi Laut</h3>
                            <p>Marine Biology</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="{{asset('template/home/images/ico/ikan2.png')}}" width="100px" height="100px">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Selam Ilmiah</h3>
                            <p>Scientific Diving</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="{{asset('template/home/images/ico/ikan2.png')}}" width="100px" height="100px">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Ekologi Laut Tropis</h3>
                            <p>Marine Tropical Ecology</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="{{asset('template/home/images/ico/ikan2.png')}}" width="100px" height="100px">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Biologi Hewan Laut</h3>
                            <p> Marine Animal Biology</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="{{asset('template/home/images/ico/ikan2.png')}}" width="100px" height="100px">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading"> Biologi Tumbuhan Laut</h3>
                            <p>Marine Botany</p>
                        </div>
                    </div>
                </div>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->
    @endsection
    @section('script')
     <script src="{{asset('template/front/js/jquery.js')}}"></script>
     <script type="text/javascript">
        $("#empat").addClass('active');
     </script>
    @endsection
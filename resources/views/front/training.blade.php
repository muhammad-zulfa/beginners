@extends('template.front.front')
@section('content')
<section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Training</h2>
                <p class="lead">Kegitan-kegiatan pelatihan</p>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>International Training Workshop on Biorock® Coral Reef Restoration dilaksanakan tanggal 12-24 Januari 2004. Pelaksana kegiatan adalah The Global Coral Reef Alliance’s and Sun & Sea e.V.’s dan Taman Sari Resort, Pemuteran, Bali.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Indonesian Marine Taxonomy, Held by Oceanography Research Center Indonesian Science Institute. May 25-30, 2009, Pari Islands</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Indonesian Marine Taxonomy, Held by Oceanography Research Center Indonesian Science Institute. June 15-27, 2009, Museum Zoologicum Bogoriense, Cibinong Bogor</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Scientific writing for Publication, Held by HAYATI Journal of Bioscience. Bogor, August 20th, 2009</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Basic Offshore Sea Survival, Held by Citra Catur Tunggal Company-Safety Training Division November, 2007</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>T-B.O.S.I.E.T. (HUET, Sea Survival, First Aid, Fire Fighting) PT. Safetindo Perkasa, Jakarta February 2010</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-7 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <!--<i class="fa fa-bullhorn"></i>-->
                            <i class="fa fa-leaf"></i>
                            <h3>Coral Finder Training for Trainers,  Mana Island, Fiji,November 2011</h3>
                        </div>
                    </div>

                </div><!--/.services-->
            </div><!--/.row-->



        </div><!--/.container-->
    </section><!--/#feature-->

@section('script')
 <script src="{{asset('template/front/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#tiga").addClass('active');
 </script>
@endsection

@endsection
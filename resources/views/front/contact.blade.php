@extends('template.front.front')
@section('content')
<section id="contact-info">
        <div class="center">
            <h2>How to Reach Me?</h2>
            <p class="lead">Contact me if you are interested with my website and want to question to me</p>
        </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=IPB&amp;aq=0&amp;oq=IPB&amp;sll=-6.5874733,106.8009584&amp;sspn=-6.5874733,106.8009584&amp;ie=UTF8&amp;hq=IPB,&amp;ll=-6.5874733,106.8009584&amp;spn=-6.5874733,106.8009584&amp;t=m&amp;z=18&amp;iwloc=A&amp;cid=1073661719450182870&amp;output=embed"></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h5>Beginer Subhan</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>  <!--/gmap_area -->

@endsection

@section('script')
 <script src="{{asset('template/front/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#sembilan").addClass('active');
 </script>
@endsection
@section('scriptmap')
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
    (function() {
        window.onload = function() {

            // Membuat konfigurasi umum peta berbasis Google Maps
            // zoom: untuk perbesaran/skala peta;
            // center: untuk menentukan titik koordinat tengah peta;
            // mapTypeId: untuk menentukan tipe peta yang digunakan;
            var options = {
                zoom: 17,
                center: new google.maps.LatLng(-6.590295,106.800945),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Membuat objek peta Google Maps, memanggil elemen HTML dengan id = 'map'
            var map = new google.maps.Map(document.getElementById('map'), options);

            // Menambahkan marker (penanda) ke dalam peta
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(-6.590295,106.800945),
                map: map,
                title: 'Click Me'
            });

            // Membuat InfoWindow dengan memunculkan informasi/teks ketika di-klik
            var infowindow = new google.maps.InfoWindow({
                content: 'This is Sudo Company Location'
            });

            // Menambahkan event Click pada penanda
            google.maps.event.addListener(marker, 'click', function() {

                // Memanggil 'open method' InfoWindow
                infowindow.open(map, marker);
            });

        };
    })();
</script>
@endsection
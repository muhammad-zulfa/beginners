@extends('template.front.front')
@section('content')
<section id="about-us">
            <div class="container">
    			<div class="center wow fadeInDown">
    				<h2>About Me</h2>
    				<p class="lead">Beginer Subhan, M.Si, pengajar di Ilmu dan Teknologi Kelautan (ITK), Fakultas perikanan dan Ilmu Kelautan (FPIK) IPB. Tamat pendidikan menegah atas di SMU Patra Dharma Pulau Bunyu, Kalimantan Timur. Gelar Sarjana Perikanan diperoleh pada tahun 2003 dari Jurusan Ilmu dan Teknologi Kelautan Fakultas Perikanan dan Ilmu Kelautan IPB. Gelar Master Sains (M.Si)  pada bidang kajian biologi laut diperoleh pada tahun 2009 dari Program Studi Ilmu Kelautan Sekolah Pasca Sarjana Institut Pertanian Bogor.
                        Kegiatan lain di sela-sela mengajar pada Program Sarjana di Departemen ITK, ia juga aktif dalam kegiatan penelitian dan pengabdian pada masyarakat. Kegiatan penelitian terapan telah dilakukan sejak tahun 2003, dengan sumber dana dari pemerintah seperti RUT-XII Menristek, Insentif Riset Terapan-Menristek dan Hibah Bersaing Dikti 2007-2008 serta kerjasama dengan pemerintah daerah dan swasta</p>
    			</div>

    			<!-- about us slider -->
    			<div id="about-slider">

    						<div id="foto">
    							<img src="{{asset('template/home/images/about/foto.jpg')}}" class="img-responsive" alt="">
    					   </div>
    					 <!--/#carousel-slider-->
    			</div><!--/#about-slider-->


    			<!-- Our Skill -->
    			<div class="skill-wrap clearfix">

    				<div class="center wow fadeInDown">
    					<h2>Kegiatan Organisasi</h2>
    					<p class="lead">Kegiatan organisasi yang diikuti</p>
    				</div>

    				<div class="row">

    					<div class="col-sm-3">
    						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
    							<div class="joomla-skill">
    								<p><em>2007-Sekarang</em></p>
    								<p>Indonesian Coral Reef  Society (INCRES)</p>
    							</div>
    						</div>
    					</div>

    					 <div class="col-sm-3">
    						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    							<div class="html-skill">
    								<p><em>2005-Sekarang</em></p>
    								<p>Ikatan Sarjana Oseanologi Indonesia (ISOI)</p>
    							</div>
    						</div>
    					</div>

    					<div class="col-sm-3">
    						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
    							<div class="css-skill">
    								<p><em>2003-Sekarang</em></p>
    								<p>Himpunan Alumni Fakultas Perikanan dan Ilmu Kelautan IPB(HA-FPIK) </p>
    							</div>
    						</div>
    					</div>

    					 <div class="col-sm-3">
    						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms">
    							<div class="wp-skill">
    								<p><em>2010-Sekarang</em></p>
    								<p>Himpunan Ahli Pengelolaan Pesisir dan Pulau-pulau Kecil Indonesia (HAPPI)</p>
    							</div>
    						</div>
    					</div>

                        <div class="col-sm-3">
                            <div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="joomla-skill">
                                    <p><em>2011</em></p>
                                    <p>Masyarakat Taksonomi Kelautan Indonesia(MATAKI)</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                                <div class="html-skill">
                                    <p><em>20011</em></p>
                                    <p>Coral Diseases Working Group-Indonesia</p>
                                </div>
                            </div>
                        </div>
    				</div>
                </div><!--/.our-skill-->
    			<!--section-->
    		</div><!--/.container-->
        </section><!--/about-us-->
@endsection

@section('script')
 <script src="{{asset('template/home/js/jquery.js')}}"></script>
 <script type="text/javascript">
    $("#dua").addClass('active');
 </script>
@endsection
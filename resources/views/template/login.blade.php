<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo (isset($pageTitle) ? $pageTitle : Config::get('app.title') )?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<!-- Google Fonts: Montserrat, Robotto, Raleway & Open Sans -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7cOpen+Sans:400,300%7cRoboto:400,100,300%7cRaleway:400,300,200,100" />
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' />
	
	<link rel="stylesheet" type="text/css" href="<?php echo asset('template/front/framework/bootstrap/3.3.2/css/bootstrap.min.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo asset('template/front/framework/font-awesome/4.3.0/css/font-awesome.min.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo asset('template/front/js/plugins/facebox/facebox.css')?>" />
	
	<link rel="stylesheet" href="<?php echo asset('template/backoffice/stylesheets/login.css')?>" />
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    <div class="container">
		<div class="logo">
			<a href="<?php echo url('backoffice')?>"><img alt="Violet LMS" src="<?php echo asset('template/front/images/logo-small.png')?>" style="height: 50px;" /></a>
		</div>
		
		@yield('content')
		
		<footer>
			<?php echo Config::get('app.title')?> Backoffice
			<div class="copyright">Copyright &copy; 2015</div>
		</footer>
	</div>
	
	<!-- JS -->
	<script type="text/javascript" src="<?php echo asset('template/backoffice/javascripts/jquery-2.1.3.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('template/backoffice/javascripts/jquery-migrate-1.2.1.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('template/front/framework/bootstrap/3.3.2/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/facebox/facebox.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/validate/jquery.validate.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/validate/localization/messages_id.min.js')?>"></script>
	
	<script type="text/javascript" src="<?php echo asset('template/front/js/app.js')?>"></script>
	
	@yield('jsscript')
</body>
</html>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo (isset($pageTitle) ? $pageTitle : Config::get('app.title') )?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <link rel="stylesheet" href="{{asset('template/front/css/jquery-ui.css')}}">


    <link rel="shortcut icon" href="{{ asset('template/front/images/favicon.png') }}" type="image/x-icon" />

	<!-- Open Sans font from Google CDN -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700%7cRaleway:400,300,200,100' rel='stylesheet' type='text/css' />

    <!-- General Stylesheets -->
	<link href="<?php echo asset('template/front/framework/bootstrap/3.3.2/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo asset('template/front/framework/font-awesome/4.3.0/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset('template/front/js/plugins/facebox/facebox.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo asset('template/front/css/poi.css')?>" rel="stylesheet" type="text/css">

	@yield('headscript')
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('route') }}"><img src="{{ asset('template/front/images/logo-small.png') }}" alt="Naik.Travel" /> </a>
            </div>
            <div class="collapse navbar-collapse navbar-left">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('route') }}">Pencarian Rute</a></li>
                    <li><a href="javascript:maintenance()">Pencarian POI</a></li>
                    <li class="hidden-sm"><a href="javascript:maintenance()">Saran</a></li>
                </ul>
            </div><!--/.nav-collapse -->

            <form action="{{url('route/home/index')}}" class="navbar-form navbar-right" id="form-search-route" role="search">
                <div class="form-group">
                    <input type="text" id="route" name="route" class="form-control" placeholder="Contoh: Cilibende ke Bantar Jati">
                </div>
                <button type="submit" class="btn btn-default hidden-xs"><i class="fa fa-search"></i></button>

            </form>

        </div>

    </div><!--/.navbar -->

    <div class="row-offcanvas row-offcanvas-left">
        <div id="sidebar">
            <div class="col-md-12">
                <!-- Sidebar Content Here -->
                @yield('sidebar')
            </div>
        </div>
        <div id="main">
            <div class="col-md-12" style="padding: 0px;">
                <div class="btn-offcanvas">
                    <button type="button" class="btn btn-warning btn-xs" data-toggle="button"><i class="fa fa-cogs"></i> Panel</button>
                </div>

                @yield('content')
            </div>
        </div>
    </div>

    <div class="footer navbar-fixed-bottom">
        <div class="container">
            <ul>
                <li><a href="/">Beranda</a></li>
                <li><a href="javascript:maintenance()">Tentang</a></li>
                <li><a href="javascript:maintenance()">Saran</a></li>
                <li>&copy; 2015 Naik.Travel</li>
            </ul>
        </div>
    </div>

<!-- JS Libs -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/front/js/libs/jquery-2.1.3.min.js">'+"<"+"/script>"); </script>
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/front/js/libs/jquery-migrate-1.2.1.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/front/js/libs/jquery-1.11.2.min.js">'+"<"+"/script>"); </script>
<![endif]-->

<script type="text/javascript" src="<?php echo asset('template/front/framework/bootstrap/3.3.2/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/validate/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/validate/localization/messages_id.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/facebox/facebox.js')?>"></script>


<script type="text/javascript" src="<?php echo asset('template/front/js/app.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle=button]').click(function() {
            $('.row-offcanvas').toggleClass('active');
        });
    });
</script>

@yield('jsscript')
</body>
</html>

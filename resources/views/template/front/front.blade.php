<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BeginerSubhan</title>

	<!-- core CSS -->
    <link href="{{asset('template/home/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/home/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/home/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/home/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('template/home/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('template/home/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{asset('template/home/js/html5shiv.js')}}"></script>
    <script src="{{asset('template/home/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('template/home/images/ico/logo-ipb.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('template/home/images/ico/logo-ipb.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('template/home/images/ico/logo-ipb.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('template/home/images/ico/logo-ipb.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('template/home/images/ico/logo-ipb.png')}}">
</head><!--/head-->

<body class="homepage">
    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +08966 947 28 38</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/beginer.subhan.1"><i class="fa fa-facebook" ></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <!--<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li>-->
                            </ul>
                            <div class="search">
                                <form role="form">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                           </div>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html{{redirect('fronthome')}}"><img src="{{asset('template/home/images/logo2.png')}}" alt="logo"></a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">

                        <li id="satu"><a href="{{url('home')}}">Home</a></li>
                        <li id="dua"><a href="{{url('home/aboutus')}}">About Me</a></li>
                        <li id="tiga"><a href="{{url('home/training')}}">Training</a></li>
                        <li id="empat"><a href="{{url('home/courses')}}">Courses</a></li>
                        <li id="lima"><a href="{{url('home/research')}}">Research</a></li>
                        <li id="enam"><a href="{{url('home/empowerement')}}">Community Empowerement</a></li>
                        <li id="tujuh"><a href="{{url('home/publication')}}">Publication</a></li>
                        <li id="delapan"><a href="{{url('home/blog')}}">Blog</a></li>
                        <li id="sembilan"><a href="{{url('home/contact')}}">Contact</a></li>

                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->

    </header><!--/header-->
    @yield('content')
    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 <a target="_blank"title="Carios Developer">Carios Developer</a>. All Rights Reserved.
                </div>

            </div>
        </div>
    </footer><!--/#footer-->

    <script src="{{asset('template/home/js/jquery.js')}}"></script>
    <script src="{{asset('template/home/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('template/home/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('template/home/js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('template/home/js/main.js')}}"></script>
    <script src="{{asset('template/home/js/wow.min.js')}}"></script>

</body>
</html>
@yield('script')
<!DOCTYPE html>

<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo (isset($pageTitle) ? $pageTitle : Config::get('app.title') )?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="<?php echo asset('template/backoffice/stylesheets/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo asset('template/backoffice/stylesheets/pixel-admin.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo asset('template/backoffice/stylesheets/widgets.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo asset('template/backoffice/stylesheets/themes.min.css')?>" rel="stylesheet" type="text/css">

    <!-- Facebox -->
	<link href="<?php echo asset('template/front/js/plugins/facebox/facebox.css')?>" rel="stylesheet" type="text/css">

    <link href="<?php echo asset('template/backoffice/stylesheets/poi.css')?>" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
		<script src="<?php echo asset('template/backoffice/javascripts/ie.min.js')?>"></script>
	<![endif]-->

	@yield('headscript')
</head>
<body class="theme-asphalt main-menu-animated main-menu-fixed main-navbar-fixed">

<script>var init = [];</script>

<div id="main-wrapper">
	<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
		<!-- Main menu toggle -->
		<button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">HIDE MENU</span></button>
		
		<div class="navbar-inner">
			<!-- Main navbar header -->
			<div class="navbar-header">

				<!-- Logo -->
				<a href="<?php echo url('backoffice')?>" class="navbar-brand">
					<?php echo Config::get('app.title')?>
				</a>

				<!-- Main navbar toggle -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>
			</div> <!-- / .navbar-header -->

			<div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
				<div>
					<ul class="nav navbar-nav">
						<li>
							<a target="_blank" href="">Beranda</a>
						</li>
						<!-- 
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu Pintas</a>
							<ul class="dropdown-menu">
								<li><a href="#">First item</a></li>
								<li><a href="#">Second item</a></li>
								<li class="divider"></li>
								<li><a href="#">Third item</a></li>
							</ul>
						</li> 
						-->
					</ul> <!-- / .navbar-nav -->

					<div class="right clearfix">
						<ul class="nav navbar-nav pull-right right-navbar-nav">
							<li>
								<form class="navbar-form pull-left">
									<input type="text" class="form-control" placeholder="Cari Tempat">
								</form>
							</li> 
							
							<li class="dropdown">
								<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
									<img src="<?php echo asset('template/backoffice/demo/avatars/1.jpg')?>" alt="">
									<span>{{ Auth::user()->fullname }}</span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Ubah Profile</a></li>
									<li><a href="#">Ganti Password</a></li>
									<!-- <li><a href="#"><i class="dropdown-icon fa fa-cog"></i>&nbsp;&nbsp;Settings</a></li> -->
									<li class="divider"></li>
									<li><a href="javascript:void(0)" title="Keluar" onclick="confirmDirectPopUp('<?php echo url('account/logout')?>', 'Konfirmasi', 'Anda ingin keluar dari aplikasi?', 'Ya', 'Tidak');"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
								</ul>
							</li>
						</ul> <!-- / .navbar-nav -->
					</div> <!-- / .right -->
				</div>
			</div> <!-- / #main-navbar-collapse -->
		</div> <!-- / .navbar-inner -->
	</div> <!-- / #main-navbar -->

	<div id="main-menu" role="navigation">
		<div id="main-menu-inner">
			<div class="menu-content top" id="menu-content-demo">
				<div>
					<div class="text-bg"><span class="text-slim">Welcome,</span> <span class="text-semibold">{{ Auth::user()->username }}</span></div>

					<img src="<?php echo asset('template/backoffice/demo/avatars/1.jpg')?>" alt="" class="">
					<div class="btn-group">
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-envelope"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-user"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-cog"></i></a>
						<a href="#" class="btn btn-xs btn-danger btn-outline dark"><i class="fa fa-power-off"></i></a>
					</div>
					<a href="#" class="close">&times;</a>
				</div>
			</div>
			<ul class="navigation">
				<li><a href="<?php echo url('backoffice/dashboard')?>"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a></li>
				<li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-users"></i><span class="mm-text">Administrasi</span></a>
					<ul>
						<li><a tabindex="-1" href="<?php echo url('backoffice/administration/users')?>"><span class="mm-text">Pengguna</span></a></li>
					</ul>
				</li>
                <li><a href="<?php echo url('backoffice/tracking')?>"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Tracking</span></a></li>
				<li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-university"></i><span class="mm-text">Blog</span></a>
					<ul>
						<li><a tabindex="-1" href="<?php echo url('backoffice/posting/post-category')?>"><span class="mm-text">Kategori Posting</span></a></li>
						<li><a tabindex="-1" href="<?php echo url('backoffice/postings')?>"><span class="mm-text">Post</span></a></li>
                        <li><a tabindex="-1" href="<?php echo url('backoffice/posting/comment')?>"><span class="mm-text">Komentar</span></a></li>
					</ul>
				</li>

			</ul> <!-- / .navigation -->
			<div class="menu-content">
				<a href="pages-invoice.html" class="btn btn-primary btn-block btn-outline dark">Create</a>
			</div>
		</div> <!-- / #main-menu-inner -->
	</div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->

	<div id="content-wrapper">
		<!-- Content -->
        @yield('content')
	</div> <!-- / #content-wrapper -->
	
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- JS Libs -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/backoffice/javascripts/jquery-2.1.3.min.js">'+"<"+"/script>"); </script>
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/backoffice/javascripts/jquery-migrate-1.2.1.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="/template/backoffice/javascripts/jquery-1.11.2.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script type="text/javascript" src="<?php echo asset('template/backoffice/javascripts/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('template/backoffice/javascripts/pixel-admin.min.js')?>"></script>

<!-- Facebox -->
<script type="text/javascript" src="<?php echo asset('template/front/js/plugins/facebox/facebox.js')?>"></script>

<script type="text/javascript">
	init.push(function () {
		$('#menu-content-demo .close').click(function () {
			var $p = $(this).parents('.menu-content');
			$p.addClass('fadeOut');

			setTimeout(function () {
			    $p.css({ height: $p.outerHeight(), overflow: 'hidden' }).animate({'padding-top': 0, height: $('#main-navbar').outerHeight()}, 500, function () {
			        $p.remove();
		    	});
		    }, 300);
			return false;
		});
	})
	
	//Notif
	init.push(function () {
		@if (count($errors) > 0)
    		@foreach ($errors->all() as $error)
    		   $.growl.error({ message: "{{ $error }}" });
			@endforeach
		@endif
		@if (Session::has('success'))
    		$.growl.notice({ message: "{{ Session::get('success') }}" });
		@endif
			
	});
	
	window.PixelAdmin.start(init);
</script>
<script src="<?php echo asset('template/backoffice/javascripts/app.js')?>"></script>

@yield('jsscript')
</body>
</html>
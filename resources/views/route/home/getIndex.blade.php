@extends('template.route')

@section('sidebar')
    {{--@foreach($getKet as $ket)
    <div class="sidecontent">
        {{'dari '.$ket->from_name.' naik '. $ket->access_name }}<br>
        {{'sampai ' .$ket->to_name .' dengan :'}}<br>
        {{'biaya Rp '. $ket->cost}}<br>
        {{'Waktu '. $ket->traveling_time . ' menit'}}
    </div>
    @endforeach--}}
@endsection

@section('content')
    <div id="map-canvas" style="background: #E9E5DC">
        <div style="text-align: center;">Loading Maps...</div>
    </div>
    <div id="tes">
        <a href="'.url('backoffice/access/access-category/edit/'.$row->cat_id).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
        <a href="javascript:maintenance()" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
    </div>
@endsection
@section('jsscript')
    <script type="text/javascript" src="{{asset('template/front/js/jquery-ui.js')}}"></script>
    <!--suppress JSJQueryEfficiency -->
    <script type="text/javascript" src="{{asset('template/front/js/typeahead.bundle.js')}}"></script>
    <script type="text/javascript">
            $(function(){
                $( "#route" ).autocomplete({
                            source: "{{url('route/home/list')}}",
                            select: function(event,ui){
                                $("#route").val(ui.item.value);
                            }
                });
            });

    </script>

    
@endsection
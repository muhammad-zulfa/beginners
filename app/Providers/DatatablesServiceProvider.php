<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\DatatablesService;

class DatatablesServiceProvider extends ServiceProvider{
    public function register(){
        $this->app->bind('datatables', function(){
            return new DatatablesService();
        });
    }
}

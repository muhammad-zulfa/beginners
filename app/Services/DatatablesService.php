<?php namespace App\Services;

class DatatablesService{
    function getOffset($request) {
    	$start = 0;
    	if (isset($request['start'])) {
    		$start = intval($request['start']);
    
    		if ($start < 0)
    			$start = 0;
    	}
    
    	return intval($start);
    }
    
    function getLimit($request) {
    	$rows = 10;
    	if (isset($request['length'])) {
    		$rows = intval($request['length']);
    		if ($rows < 5 || $rows > 500) {
    			$rows = 10;
    		}
    	}
    
    	return intval($rows);
    }
    
    
    function getSortDir($request) {
    	$sort_dir = "ASC";
    	$sdir = strip_tags($request['order'][0]['dir']);
    	if (isset($sdir)) {
    		if ($sdir != "asc" ) {
    			$sort_dir = "DESC";
    		}
    	}
    
    	return $sort_dir;
    }
    
    function getSortCol($request, $cols) {
    	$sCol = $request['order'][0]['column'];
    	$col = 0;
    	
    	if (isset($sCol)) {
    		$col = intval($sCol);
    		if ($col < 0 || $col > (count($cols) - 1))
    			$col = 0;
    	}
    	$colName = $cols[$col];
    
    	return $colName;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 07/07/2015
 * Time: 4:48
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Posting extends Model {
protected $table = 'post';
    protected $primaryKey = 'id_post';
    public $timestamps = true;
    protected $fillable = ['title','label','isi','group'];

    public function Posting(){
        $this->belongsTo('App\Models\Category','group_id');
        $this->belongsTo('App\Models\Commentar','id_post');
    }
    public function listData($offset, $limit, $search, $sortCol, $sortDir){
        if($sortCol != "")
            $query = Posting::orderBy($sortCol, $sortDir);
        else
            $query = Posting::orderBy("updated_at", "asc");

        if($search != ""){
            $query->where("title", "LIKE", "%".$search."%");
            $query->orWhere("group.group_name", "LIKE", "%".$search."%");
        }

        if($limit == -1)return $query->get();
        else return $query->take($limit)->skip($offset)->get();
    }

    public function countListData($search = ""){
        return $this->all()->count();
    }
}
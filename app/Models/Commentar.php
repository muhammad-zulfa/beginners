<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 14/07/2015
 * Time: 18:20
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Commentar extends Model
{
    protected $table = 'komentar';
    protected $timestamps = true;
    protected $fillable = ['id_post', 'commenters_email', 'commenters_name', 'comment', 'reply_to'];
    public function Commentar(){
        $this->hasMany('App\Models\Posting','id_post');
    }
    public function listData($offset, $limit, $search, $sortCol, $sortDir){
        if($sortCol != "")
            $query = Commentar::orderBy($sortCol, $sortDir);
        else
            $query = Commentar::orderBy("commenters_email", "asc");

        if($search != ""){
            $query->where("commenters_email", "LIKE", "%".$search."%");
            $query->orWhere("commenters_name", "LIKE", "%".$search."%");
        }

        if($limit == -1)return $query->get();
        else return $query->take($limit)->skip($offset)->get();
    }

    public function countListData($search = ""){
        return $this->all()->count();
    }
}
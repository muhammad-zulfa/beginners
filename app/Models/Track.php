<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 08/07/2015
 * Time: 22:19
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Track  extends Model{
    protected $table = "tracking";
    protected $primaryKey=null;
    public $timestamps = false;
    protected $fillable = ["tracking_ip","tanggal","jumlah"];

    public function listData($offset, $limit, $search, $sortCol, $sortDir){
        if($sortCol != "")
            $query = Track::orderBy($sortCol, $sortDir);
        else
            $query = Track::orderBy("tanggal", "asc");

        if($search != ""){
            $query->where("tracking_ip", "LIKE", "%".$search."%");
            $query->orWhere("tanggal", "LIKE", "%".$search."%");
        }

        if($limit == -1)return $query;
        else return $query->take($limit)->skip($offset);
    }

    public function countListData($search = ""){
        return $this->all()->count();
    }
}
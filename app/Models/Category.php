<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 07/07/2015
 * Time: 4:39
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model{

    protected $table = 'group';
    protected $primaryKey = 'group_id';
    protected $fillable = ['group_name'];
    public $timestamps = false;
    public function Category(){
        $this->hasMany('App\Models\Posting','group_id');
    }
    public function listData($offset, $limit, $search, $sortCol, $sortDir){
        if($sortCol != "")
            $query = Category::orderBy($sortCol, $sortDir);
        else
            $query = Category::orderBy("group_name", "asc");

        if($search != ""){
            $query->where("group_name", "LIKE", "%".$search."%");
            $query->orWhere("group_id", "LIKE", "%".$search."%");
        }

        if($limit == -1)return $query;
        else return $query->take($limit)->skip($offset);
    }

    public function countListData($search = ""){
        return $this->all()->count();
    }
}
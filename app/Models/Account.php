<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Account extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'account';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['fullname', 'email', 'password', 'username', 'bio', 'phone'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	/**
	 * Check username
	 * @param String $username
	 * @param String $id
	 * @return Object
	 */
	public static function checkUsername($username, $id = ''){
	    $data = Account::where('username', '=', $username);
	    
	    if($id != "")$data->where('id', '<>', $id);
	    
	    return $data;
	}
	
	/**
	 * 
	 * @param String $email
	 * @param String $id
	 * @return Object
	 */
	public static function checkEmail($email, $id = ''){
	    $data = Account::where('email', '=', $email);
	     
	    if($id != "")$data->where('id', '<>', $id);
	     
	    return $data;
	}
	
	/**
	 * Get list user
	 * @param unknown $offset
	 * @param unknown $limit
	 * @param unknown $search
	 * @param unknown $sortCol
	 * @param unknown $sortDir
	 * @return unknown
	 */
	public function listUser($offset, $limit, $search, $sortCol, $sortDir){

        if($sortCol != "")
            $query = Account::orderBy($sortCol, $sortDir);
        else
            $query = Account::orderBy("created_at", "asc");

        if($search != ""){
            $query->where("username", "LIKE", "%".$search."%");
            $query->orWhere("fullname", "LIKE", "%".$search."%");
        }

        if($limit == -1)return $query;
        else return $query->take($limit)->skip($offset);
	}
	
	/**
	 * Count list of user
	 * @param string $search
	 * @return number
	 */
	public function countListUser($search = ""){
	    return $this->all()->count();
	}

}

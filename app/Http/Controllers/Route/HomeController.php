<?php namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Models\AccessEdge;
use App\Models\Node;
use App\Services\DatatablesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller {

	public function __construct(){

	}
    public function getRoute(Request $request,AccessEdge $accessEdge){
        $search = $request->get('route');
        $get = str_replace("+"," ",$search);
        $potong=implode(" ", array_slice(explode(" ", $get), 2, 1));
        $potong2=implode(" ", array_slice(explode(" ", $get), 0, 1));
        $lnglatfrom = [];
        $dat = [];
        $lnglattuj = [];
        if($search!=null){
            $lnglatfrom = $accessEdge->getNodeByName($potong2) ;
            $lnglattuj = $accessEdge->getNodeByName($potong);
            $dat = AccessEdge::select('ac.*','from.name as from_name','to.name as to_name')
                ->join('access as ac','access_edge.access_id','=','ac.access_id')
                ->join('node as from','access_edge.node_from','=','from.node_id')
                ->join('node as to','access_edge.node_to','=','to.node_id')
                ->where(function($q) use($potong2,$potong){
                    $q->where('from.name','=',$potong2)
                        ->where('to.name','=',$potong);
                })->get();
        }
        $graph = array();
        $graph = [
          $dat->frome_name
        ];
    }
	public function getIndex(){

	    $data = [
	        'pageTitle' => 'Route'
        ];

	    return view('route.home.getIndex', $data);
	}
    public function getList(Request $request){
        // variable initializatio

        if($request->get('route')==null){
        $term = Input::get('term');
            $potong=implode(" ", array_slice(explode(" ", $term), 2, 1));
            $potong2=implode(" ", array_slice(explode(" ", $term), 0, 0));
        // run query to get Data listing
        //$listData = \DB::table('access_edge')->selectRaw("from.name + ' ke ' + to.name as direct")
            $access = AccessEdge::select('from.name as fromname','to.name as toname')//->where('node_from','like','%1%');
            ->join('access','access_edge.access_id','=','access.access_id')
            ->join('node as from','access_edge.node_from','=','from.node_id')
            ->join('node as to','access_edge.node_to','=','to.node_id')
            ->where('from.name','like','%'.$term.'%')
                ->where('to.name', 'like' ,'%'.$potong.'%')
                ->orWhere('to.name', 'like' ,'%'.$term.'%')
                ->groupBy('from.name','to.name')
            ->get();
        $data = array();
        foreach($access as $row)
        {
            $data[] = $row->fromname. ' ke '. $row->toname;
        }
        // get result after running query and put it in array

        // format it to JSON, this output will be displayed in datatable
        return response()->json($data);
        }
        else
        {
            $search = $request->get('route');
            $i=2;
            $how=true;
            while($how==true){

                $get = substr($search,1,$i);
                $node = Node::select('name','longitude','latitude')
                    ->where('name','like','%'.$get.'%')
                    ->first();
                if(count($node) == 1){
                $get2 = substr($search,strlen($node->name)+5,$i);
                    $node2 = Node::select('name','longitude','latitude')
                        ->where('name','like','%'.$get2.'%')
                        ->first();
                    $datafrom = [
                  'getFrom' => $node,
                    'getTo' => $node2
                ];

                    return view('route.home.getIndex',$datafrom);
                }else{$i++;}

            }


        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 07/07/2015
 * Time: 6:17
 */

namespace App\Http\Controllers\Backoffice\Posting;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Posting;
use App\Services\DatatablesService;
use Illuminate\Http\Request;

class PostingController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getIndex(){
        $data = [
            'pageTitle' => 'Kategori Posting'
        ];

        return view('backoffice.posting.getIndex', $data);
    }

    public function getAjaxList(DatatablesService $datatables, Request $request, Posting $category){
        // variable initialization
        $search = "";
        $start = 0;
        $rows = 10;

        // limit
        $start = $datatables->getOffset($request);
        $rows = $datatables->getLimit($request);

        // get search value (if any)
        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $search = $request['search']['value'];
        }

        // sort
        $sortDir = $datatables->getSortDir($request);
        $sortCol = $datatables->getSortCol($request, array("", "title","label","group"));

        // run query to get Data listing
        $listData = $category->listData($start, $rows, $search, $sortCol, $sortDir);
        $recordsTotal = $category->countListData();

        if($search != "")$recordsFiltered = $category->countListData($search);
        else $recordsFiltered = $recordsTotal;

        /*
         * Output
         */
        $output = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => array()
        );

        // get result after running query and put it in array
        $no = $start+1;
        foreach ($listData as $row) {
            //$data = \DB::table('group')->select("group_name")->where('group_id', '=', $row->group)->get();
            $record = array();
            $record[] = $no++;
            $record[] = $row->title;
            $record[] = $row->label;
            $record[] = $row->group;
            $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('backoffice/postings/edit/'.$row->id_post).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" onclick="confirmDirectPopUp(\''.url('backoffice/postingsy/delete/'.$row->id_post).'\', \'Konfirmasi\', \'Apakah anda yakin ingin menghapus?\', \'Ya, Hapus Data\', \'Tidak\');" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
	               </div>
	            ';

            $output['data'][] = $record;
        }
        // format it to JSON, this output will be displayed in datatable
        return response()->json($output);
    }

    public function getAdd(Category $category){
        $data = [
            'pageTitle' => 'Tambah Data',
            'kategori' => $category->all()
        ];

        return view('backoffice.posting.getAdd', $data);
    }

    public function getEdit($id){
        try {
            $obj = Posting::find($id);

            if(count($obj) > 0){
                $data = [
                    'pageTitle' => 'Ubah Data',
                    'obj'       => $obj
                ];

                return view('backoffice.posting.getEdit', $data);
            }else{
                return redirect('backoffice/postings')->withError([
                    'Data tidak ditemukan.'
                ]);
            }
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/postings')->withError([
                'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
            ]);
        }
    }

    public function getDelete($id){
        try {
            $obj = Posting::find($id);

            if(count($obj) > 0){
                $obj->delete();

                return redirect('backoffice/postings')->with('success', 'Data berhasil dihapus.');
            }else{
                return redirect('backoffice/postings')->withError([
                    'Data tidak ditemukan.'
                ]);
            }
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/postings')->withError([
                'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
            ]);
        }
    }

    public function postSubmit(Request $request){
        try {
            $id = $request->input('post_id');

            if($id != ""){
                $category = Posting::find($id);

                if(count($category) == 0){
                    return redirect('backoffice/postings')->withError([
                        'Data tidak ditemukan.'
                    ]);
                }
            }else{
                $category = new Posting();
            }

            $category->title = $request->input('judul');
            $category->label = $request->input('label');
            $category->group = $request->input('group');
            $category->isi = $request->input('post');
            $category->save();

            return redirect('backoffice/postings')->with('success', 'Data berhasil disimpan.');
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting')->withErrors([
                'Gagal menyimpan data. Ulangi beberapa saat lagi.'
            ]);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 07/07/2015
 * Time: 5:03
 */

namespace App\Http\Controllers\Backoffice\Posting;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\DatatablesService;
use Illuminate\Http\Request;

class CategoryController extends Controller {
    public function __construct(){
        $this->middleware('auth');
    }
    public function getIndex(){
        $data = [
            'pageTitle' => 'Kategori Posting'
        ];

        return view('backoffice.posting.post-category.getIndex', $data);
    }

    public function getAjaxList(DatatablesService $datatables, Request $request, Category $category){
        // variable initialization
        $search = "";
        $start = 0;
        $rows = 10;

        // limit
        $start = $datatables->getOffset($request);
        $rows = $datatables->getLimit($request);

        // get search value (if any)
        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $search = $request['search']['value'];
        }

        // sort
        $sortDir = $datatables->getSortDir($request);
        $sortCol = $datatables->getSortCol($request, array("", "group_name"));

        // run query to get Data listing
        $listData = $category->listData($start, $rows, $search, $sortCol, $sortDir);
        $recordsTotal = $category->countListData();

        if($search != "")$recordsFiltered = $category->countListData($search);
        else $recordsFiltered = $recordsTotal;

        /*
         * Output
         */
        $output = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => array()
        );

        // get result after running query and put it in array
        $no = $start+1;
        foreach ($listData->get() as $row) {
            $record = array();

            $record[] = $no++;
            $record[] = $row->group_name;
            $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('backoffice/posting/post-category/edit/'.$row->group_id).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" onclick="confirmDirectPopUp(\''.url('backoffice/posting/post-category/delete/'.$row->group_id).'\', \'Konfirmasi\', \'Apakah anda yakin ingin menghapus?\', \'Ya, Hapus Data\', \'Tidak\');" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
	               </div>
	            ';

            $output['data'][] = $record;
        }

        // format it to JSON, this output will be displayed in datatable
        return response()->json($output);
    }

    public function getAdd(){
        $data = [
            'pageTitle' => 'Tambah Data'
        ];

        return view('backoffice.posting.post-category.getAdd', $data);
    }

    public function getEdit($id){
        try {
            $obj = Category::find($id);

            if(count($obj) > 0){
                $data = [
                    'pageTitle' => 'Ubah Data',
                    'obj'       => $obj
                ];

                return view('backoffice.posting.post-category.getEdit', $data);
            }else{
                return redirect('backoffice/posting/post-category')->withError([
                    'Data tidak ditemukan.'
                ]);
            }
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting/post-category')->withError([
                'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
            ]);
        }
    }

    public function getDelete($id){
        try {
            $obj = Category::find($id);

            if(count($obj) > 0){
                $obj->delete();

                return redirect('backoffice/posting/post-category')->with('success', 'Data berhasil dihapus.');
            }else{
                return redirect('backoffice/posting/post-category')->withError([
                    'Data tidak ditemukan.'
                ]);
            }
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting/post-category')->withError([
                'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
            ]);
        }
    }

    public function postSubmit(Request $request){
        try {
            $id = $request->input('group_id');

            if($id != ""){
                $category = Category::find($id);

                if(count($category) == 0){
                    return redirect('backoffice/posting/post-category')->withError([
                        'Data tidak ditemukan.'
                    ]);
                }
            }else{
                $category = new Category();
            }

            $category->group_name = $request->input('name');
            $category->save();

            return redirect('backoffice/posting/post-category')->with('success', 'Data berhasil disimpan.');
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting/post-category')->withErrors([
                'Gagal menyimpan data. Ulangi beberapa saat lagi.'
            ]);
        }
    }
}
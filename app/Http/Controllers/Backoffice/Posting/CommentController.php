<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 14/07/2015
 * Time: 18:12
 */
namespace App\Http\Controllers\Backoffice\Posting;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Commentar;
use App\Models\Posting;
use App\Services\DatatablesService;
use Illuminate\Http\Request;

class CommentController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getIndex(){
        $data = [
            'pageTitle' => 'Komentar Posting'
        ];

        return view('backoffice.posting.comment.getIndex', $data);
    }

    public function getAjaxList(DatatablesService $datatables, Request $request, Commentar $category){
        // variable initialization
        $search = "";
        $start = 0;
        $rows = 10;

        // limit
        $start = $datatables->getOffset($request);
        $rows = $datatables->getLimit($request);

        // get search value (if any)
        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $search = $request['search']['value'];
        }

        // sort
        $sortDir = $datatables->getSortDir($request);
        $sortCol = $datatables->getSortCol($request, array("","id_post", "commenters_email","commenters_name","comment","reply_to"));

        // run query to get Data listing
        $listData = $category->listData($start, $rows, $search, $sortCol, $sortDir);
        $recordsTotal = $category->countListData();

        if($search != "")$recordsFiltered = $category->countListData($search);
        else $recordsFiltered = $recordsTotal;

        /*
         * Output
         */
        $output = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => array()
        );

        // get result after running query and put it in array
        $no = $start+1;
        foreach ($listData as $row) {
            $record = array();
            $record[] = $no++;
            $record[] = $row->id_post;
            $record[] = $row->commenters_email;
            $record[] = $row->commenters_name;
            $record[] = $row->comment;
            $record[] = $row->reply_to;
            $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('backoffice/postings/edit/'.$row->id_post).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" onclick="confirmDirectPopUp(\''.url('backoffice/postingsy/delete/'.$row->id_post).'\', \'Konfirmasi\', \'Apakah anda yakin ingin menghapus?\', \'Ya, Hapus Data\', \'Tidak\');" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
	               </div>
	            ';

            $output['data'][] = $record;
        }

        // format it to JSON, this output will be displayed in datatable
        return response()->json($output);
    }

    public function getReply($postid,$id){
        $post = Posting::find($postid);
        if(count($post) > 0){
            $comment = Comment::find($id);
            if(count($comment) > 0){
                $data = [
                    "getPost" => $postid,
                ];
                return view('backoffice.posting.comment',$data);
            }
            else{
                return redirect('backoffice/posting/comment')->withError([
                    'Commentar tidak ditemukan atau sudah tidak tersedia.'
                ]);
            }
        }
        else{
                return redirect('backoffice/posting/post-category')->withError([
                    'Postingan tidak ditemukan atau sudah tidak tersedia'
                ]);
            }
        }



    public function getDelete($id){
        try {
            $obj = Category::find($id);

            if(count($obj) > 0){
                $obj->delete();

                return redirect('backoffice/posting/post-category')->with('success', 'Data berhasil dihapus.');
            }else{
                return redirect('backoffice/posting/post-category')->withError([
                    'Data tidak ditemukan.'
                ]);
            }
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting/post-category')->withError([
                'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
            ]);
        }
    }

    public function postSubmit(Request $request){
        try {
            $id = $request->input('group_id');

            if($id != ""){
                $category = Comment::find($id);

                if(count($category) == 0){
                    return redirect('backoffice/posting/comment')->withError([
                        'Data tidak ditemukan.'
                    ]);
                }
            }else{
                $category = new Comment();
            }

            $category->post_id = $request->input('id_post');
            $category->commenters_email = $request->input('email');
            $category->commenters_name = $request->input('name');
            $category->comment = $request->input('comment');
            $category->reply_to = $request->input('reply_to');
            $category->save();

            return redirect('backoffice/posting/comment')->with('success', 'Data berhasil disimpan.');
        } catch (QueryException $e) {
            \Log::error($e->getMessage());

            return redirect('backoffice/posting/comment')->withErrors([
                'Gagal menyimpan data. Ulangi beberapa saat lagi.'
            ]);
        }
    }
}
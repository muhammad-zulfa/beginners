<?php
/**
 * Created by PhpStorm.
 * User: carios-corp
 * Date: 08/07/2015
 * Time: 22:16
 */

namespace App\Http\Controllers\Backoffice;


use App\Http\Controllers\Controller;
use App\Models\Track;
use App\Services\DatatablesService;
use Illuminate\Http\Request;

class TrackController  extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getIndex(){
        $data = [
            "pageTitle" => 'Tracking'
        ];
        return view('backoffice.tracking.getIndex',$data);
    }

    public function getAjaxList(DatatablesService $datatables, Request $request, Track $category){
        // variable initialization
        $search = "";
        $start = 0;
        $rows = 10;

        // limit
        $start = $datatables->getOffset($request);
        $rows = $datatables->getLimit($request);

        // get search value (if any)
        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $search = $request['search']['value'];
        }

        // sort
        $sortDir = $datatables->getSortDir($request);
        $sortCol = $datatables->getSortCol($request, array("", "tracking_ip","tanggal","jumlah"));

        // run query to get Data listing
        $listData = $category->listData($start, $rows, $search, $sortCol, $sortDir);
        $recordsTotal = $category->countListData();

        if($search != "")$recordsFiltered = $category->countListData($search);
        else $recordsFiltered = $recordsTotal;

        /*
         * Output
         */
        $output = array(
            "draw" => intval($request['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => array()
        );

        // get result after running query and put it in array
        $no = $start+1;
        foreach ($listData->get() as $row) {
            $record = array();

            $record[] = $no++;
            $record[] = $row->tracking_ip;
            $record[] = $row->tanggal;
            $record[] = $row->jumlah;
            $output['data'][] = $record;
        }

        // format it to JSON, this output will be displayed in datatable
        return response()->json($output);
    }
}
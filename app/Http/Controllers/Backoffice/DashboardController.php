<?php namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

	public function __construct(){
		
	}
    
	public function getIndex(){
	    $data = [
	        'pageTitle' => 'Dashboard'
	    ];
	    
	    return view('backoffice.dashboard.getIndex', $data);
	}

}

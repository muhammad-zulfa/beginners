<?php namespace App\Http\Controllers\Backoffice\Administration;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Services\DatatablesService;
use Illuminate\Database\QueryException;

class UsersController extends Controller {
    
    public function __construct(){
		
	}
	
	public function getIndex(){
	    $data = [
	        'pageTitle' => 'Daftar Pengguna'
	    ];
	    
	    return view('backoffice.administration.users.getIndex', $data);
	}
	
	public function getTest($id){
	    try {
	        echo "<pre>";
	        echo Account::find($id);
	        echo "</pre>";
	        
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	         
	        return redirect('backoffice/administration/users')->withError([
	            'Data berhasil disimpan.'
	        ]);
	    }
	}
	
	public function getAjaxList(DatatablesService $datatables, Request $request, Account $account){
	    // variable initialization
	    $search = "";
	    $start = 0;
	    $rows = 10;
	     
	    // limit
	    $start = $datatables->getOffset($request);
	    $rows = $datatables->getLimit($request);
	    
	    // get search value (if any)
	    if ( isset($request['search']) && $request['search']['value'] != '' ) {
	        $search = $request['search']['value'];
	    }
	    
	    // sort
	    $sortDir = $datatables->getSortDir($request);
	    $sortCol = $datatables->getSortCol($request, array("", "fullname", "username", "email"));
	    
	    // run query to get user listing
	    $listData = $account->listUser($start, $rows, $search, $sortCol, $sortDir);
	    $recordsTotal = $account->countListUser();
	    
	    if($search != "")$recordsFiltered = $account->countListUser($search);
	    else $recordsFiltered = $recordsTotal;
	    
	    /*
	     * Output
	     */
	    $output = array(
	        "draw" => intval($request['draw']),
	        "recordsTotal" => $recordsTotal,
	        "recordsFiltered" => $recordsFiltered,
	        "data" => array()
	    );
	    
	    // get result after running query and put it in array
	    $no = $start+1;
	    foreach ($listData->get() as $row) {
	        $record = array();
	    
	        $record[] = $no++;
	        $record[] = $row->fullname;
	        $record[] = $row->username;
	        $record[] = $row->email;
	         
	        if($row->id != 1)
	            $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('backoffice/administration/users/edit/'.$row->id).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" onclick="confirmDirectPopUp(\''.url('backoffice/administration/users/delete/'.$row->id).'\', \'Konfirmasi\', \'Apakah anda yakin ingin menghapus user: '.$row->fullname.'\', \'Ya, Hapus user\', \'Tidak\');" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
	               </div>
	            ';
	        else
	            $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('backoffice/administration/users/edit/'.$row->id).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" title="Hapus" class="btn btn-xs btn-default disabled"><i class="fa fa-trash"></i></a>
	               </div>
	            ';
	    
	        $output['data'][] = $record;
	    }
	     
	    // format it to JSON, this output will be displayed in datatable
	    return response()->json($output);
	}
	
	public function getAdd(){
	    $data = [
	        'pageTitle' => 'Tambah Pengguna'
	    ];
	     
	    return view('backoffice.administration.users.getAdd', $data);
	}
	
	public function getEdit($id){
	    try {
	        $obj = Account::find($id);
	        
	        if(count($obj) > 0){
	            $data = [
	                'pageTitle' => 'Ubah Pengguna',
	                'obj'       => $obj
	            ];
	            
	            return view('backoffice.administration.users.getEdit', $data);
	        }else{
	            return redirect('backoffice/administration/users')->withError([
	                'Data tidak ditemukan.'
	            ]);
	        }
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	    
	        return redirect('backoffice/administration/users')->withError([
	            'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
	        ]);
	    }
	}
	
	public function getDelete($id){
	    try {
	        $obj = Account::find($id);
	         
	        if(count($obj) > 0){
	            $obj->delete();
	            
	            return redirect('backoffice/administration/users')->with('success', 'Data berhasil dihapus.');
	        }else{
	            return redirect('backoffice/administration/users')->withError([
	                'Data tidak ditemukan.'
	            ]);
	        }
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	         
	        return redirect('backoffice/administration/users')->withError([
	            'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
	        ]);
	    }
	}
	
	public function getCheckUsername(Request $request, $id = ""){
	    if($request->get('username') != "" && Account::checkUsername($request->get('username'), $id)->count())
	        return "false";
	    else
	        return "true";
	}
	
	public function getCheckEmail(Request $request, $id = ""){
	    if($request->get('email') != "" && Account::checkEmail($request->get('email'), $id)->count())
	        return "false";
	    else
	        return "true";
	}
	
	public function postSubmit(Request $request){
	    try {
	        if($request->id != ""){
	            $account = Account::find($request->id);
	            
	            if(count($account) == 0){
	                return redirect('backoffice/administration/users')->withError([
	                    'Data tidak ditemukan.'
	                ]);
	            }
	        }else{
	            $account = new Account();
	        }
	         
	        $account->username = $request->input('username');
	        $account->email    = $request->input('email');
	        if($request->input('password') != "")
                $account->password = bcrypt($request->input('password'));
	        $account->fullname = $request->input('fullname');
	        $account->bio      = $request->input('bio');
	        $account->phone    = $request->input('phone');
	         
	        $account->save();
	         
	        return redirect('backoffice/administration/users')->with('success', 'Data berhasil disimpan.');
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	         
	        return redirect('backoffice/administration/users')->withError([
	            'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
	        ]);
	    }
	}
}

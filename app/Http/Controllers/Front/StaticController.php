<?php
/**
 * Created by PhpStorm.
 * User: haris
 * Date: 10/07/2015
 * Time: 6:04
 */

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\Posting;
use App\Models\Track;

class StaticController extends Controller{
    public function __construct(){

    }
    public function getFront(){
        return view('front.front');
    }
    public function getPublication(){
        return view('front.publication');
    }
    public function getEmpowerement(){
        return view('front.empowerement');
    }
    public function getContact(){
        return view('front.contact');
    }
    public function getIndex(Track $track){
        /*$ip = $_SERVER['remote_addr'];
        $count = $track->countListData("'".$ip."'");
        if($count > 0){

        }
        $track->tracking_ip = $ip;
        $track->jumlah = $count;
        $track->save();*/
        $data = [
            'link'=>'satu'
        ];
        return view('front.front',$data);
    }
    public function getBlog(Posting $post){
        $data =[
            'posting'=>$post->orderBy('updated_at','asc')->get()
        ];
        return view('front.blog',$data);
    }
    public function getAboutus(){
        return view('front.aboutus');
    }
    public function getCourses(){
        return view('front.course');
    }
    public function getTraining(){
        return view('front.training');
    }
    public function getBlogitem(){
        return view('front.blogitem');
    }
    public function getResearch(){
        return view('front.research');
    }

}
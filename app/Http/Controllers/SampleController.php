<?php namespace App\Http\Controllers;

use App\Models\Sample;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Services\DatatablesService;

class SampleController extends Controller {

	public function __construct(){
		$this->middleware('auth');
	}

	public function getIndex(){
	    $data = [
	        'pageTitle' => 'Sample Page'
	    ];
	    
	    return view('sample.getIndex', $data);
	}
	
	public function getAjaxList(DatatablesService $datatables, Request $request, Sample $sample){
	    // variable initialization
	    $search = "";
	    $start = 0;
	    $rows = 10;
	
	    // limit
	    $start = $datatables->getOffset($request);
	    $rows = $datatables->getLimit($request);
	     
	    // get search value (if any)
	    if ( isset($request['search']) && $request['search']['value'] != '' ) {
	        $search = $request['search']['value'];
	    }
	     
	    // sort
	    $sortDir = $datatables->getSortDir($request);
	    $sortCol = $datatables->getSortCol($request, array("", "name", "desc"));
	     
	    // run query to get Data listing
	    $listData = $sample->listData($start, $rows, $search, $sortCol, $sortDir);
	    $recordsTotal = $sample->countListData();
	     
	    if($search != "")$recordsFiltered = $sample->countListData($search);
	    else $recordsFiltered = $recordsTotal;
	     
	    /*
	     * Output
	     */
	    $output = array(
	        "draw" => intval($request['draw']),
	        "recordsTotal" => $recordsTotal,
	        "recordsFiltered" => $recordsFiltered,
	        "data" => array()
	    );
	     
	    // get result after running query and put it in array
	    $no = $start+1;
	    foreach ($listData->get() as $row) {
	        $record = array();
	         
	        $record[] = $no++;
	        $record[] = $row->name;
	        $record[] = $row->desc;
	
	        $record[] = '
	               <div class="btn-group">
	                   <a href="'.url('sample/edit/'.$row->sample_id).'" title="Ubah" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
	                   <a href="javascript:void(0)" onclick="confirmDirectPopUp(\''.url('sample/delete/'.$row->sample_id).'\', \'Konfirmasi\', \'Apakah anda yakin ingin menghapus?\', \'Ya, Hapus Data\', \'Tidak\');" title="Hapus" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></a>
	               </div>
	            ';
	         
	        $output['data'][] = $record;
	    }
	
	    // format it to JSON, this output will be displayed in datatable
	    return response()->json($output);
	}
	
	public function getAdd(){
	    $data = [
	        'pageTitle' => 'Tambah Data'
	    ];
	     
	    return view('sample.getAdd', $data);
	}
	
	public function getEdit($id){
	    try {
	        $obj = Sample::find($id);
	         
	        if(count($obj) > 0){
	            $data = [
	                'pageTitle' => 'Ubah Data',
	                'obj'       => $obj
	            ];
	             
	            return view('sample.getEdit', $data);
	        }else{
	            return redirect('sample')->withErrors([
	                'Data tidak ditemukan.'
	            ]);
	        }
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	         
	        return redirect('sample')->withErrors([
	            'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
	        ]);
	    }
	}
	
	public function getDelete($id){
	    try {
	        $obj = Sample::find($id);
	
	        if(count($obj) > 0){
	            $obj->delete();
	             
	            return redirect('sample')->with('success', 'Data berhasil dihapus.');
	        }else{
	            return redirect('sample')->withErrors([
	                'Data tidak ditemukan.'
	            ]);
	        }
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	
	        return redirect('sample')->withErrors([
	            'Telah terjadi sesuatu kesalahan. Silahkan ulangi beberapa saat lagi atau hubungi administrator.'
	        ]);
	    }
	}
	
	public function postSubmit(Request $request){
	    try {
	        $id = $request->input('sample_id');
	        
	        if($id != ""){
	            $sample = Sample::find($id);
	            
	            if(count($sample) == 0){
	                return redirect('sample')->withErrors([
	                    'Data tidak ditemukan.'
	                ]);
	            }
	        }else{
	            $sample = new Sample();
	        }
	    
	        $sample->name = $request->input('name');
	        $sample->desc = $request->input('desc');
	    
	        $sample->save();
	        
	        return redirect('sample')->with('success', 'Data berhasil disimpan.');
	    } catch (QueryException $e) {
	        \Log::error($e->getMessage());
	        
	        return redirect('sample')->withErrors([
	            'Gagal menyimpan data. Ulangi beberapa saat lagi.'
	        ]);
	    }
	}
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){
    return Redirect::to('home');
});
Route::group(['prefix' => 'home'],function(){
    Route::controllers([
        '/' => 'Front\StaticController'
    ]);
});
Route::get('blank', 'WelcomeController@blank');
Route::get('pwd', 'WelcomeController@pwd');

/*
 * Auth Routes
 */
Route::controllers([
    'account' => 'Account\AuthController',
    'password' => 'Account\PasswordController',
]);
/*
 * Backoffice Routes
 */
Route::group(['prefix' => 'backoffice', 'middleware' => 'auth'], function(){
    Route::controllers([
        'dashboard'             => 'Backoffice\DashboardController',
        'administration/users'  => 'Backoffice\Administration\UsersController',
        'posting/post-category' => 'Backoffice\Posting\CategoryController',
        'postings' => 'Backoffice\Posting\PostingController',
        'tracking' => 'Backoffice\TrackController',
        'posting/comment' => 'Backoffice\Posting\CommentController'
    ]);
});/*
Route::group(['prefix' => '/'], function(){
    Route::controllers([
        'home'             => 'Front\HomeController',
        'administration/users'  => 'Backoffice\Administration\UsersController',
        'posting/post-category' => 'Backoffice\Posting\CategoryController',
        'postings' => 'Backoffice\Posting\PostingController',
        'tracking' => 'Backoffice\TrackController'
    ]);
});*/
Route::get('post',function(){
   return view('posting.getIndex');
});
Route::get('backoffice', function() {
    return Redirect::to('backoffice/dashboard');
});
Route::get('reg',function(){
   return view('account.register');
});

/*
 * Sample Routes
 */
Route::controllers([
    'sample' => 'SampleController'
]);

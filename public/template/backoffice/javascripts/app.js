/* ************************************************ */
/* JW BACK END										*/
/* Created by: Efriandika Pratama					*/
/* Version: 1.0										*/
/* Email: efriandika@jakartawebs.com				*/
/* ************************************************ */

$(document).ready(function (){
	//Default validate tag
	$('#validate').validate();
});

/**
 * Wizard for bootstrap 3+
 * Developed By: Efriandika Pratama
 */
$.fn.extend({
	EpWizard : function(options) {

		var defaults = {
				step: '1',
				stepText: 'Step'
		};
		
		// Extend default options
		var opts = $.extend(defaults, options);
		var container = $(this);
		var elementParent = $(this).find('ul');
		var element = $(this).find('ul li');
		
		container.find('div.ep-wizard-step').remove();
		
		element.each(function( index, value ) {
			//alert( index + ": " + $(value).html() );
			var className = "disabled";
			
			if(index < opts.step - 1)className = 'complete';
			else if(index == opts.step - 1)className = 'active';
			
			if(index == 0)className = className + ' first';
			
			var tmp = '<div class="col-xs-2 ep-wizard-step '+className+'">' +
			               '<div class="text-center ep-wizard-stepnum">'+opts.stepText + ' ' + (index+1)+'</div>' +
			               '<div class="progress"><div class="progress-bar"></div></div>' +
			                    '<a href="#" class="ep-wizard-dot"></a>' +
			               '<div class="ep-wizard-info text-center hidden-xs">'+ $(value).html() +'</div>' +
			           '</div>';
			
			container.append(tmp);
		});
	}
});

function load(page,div){
	jQuery.ajax({
        url: page,
        beforeSend: function(){
        	jQuery(div).html('<div class="loader">Loading...</div>');
        },
        success: function(response){
            jQuery(div).html(response);
        },
        dataType:"html"
    });
    return false;
}

function load_into_box(page, dt){
	jQuery.ajax({ 
        url: page,
        data: dt,
        beforeSend: function(){
        	jQuery.facebox('<div class="loader">Loading...</div>');
        },
        success: function(response){
            jQuery.facebox(response);
        },
        type:"post",
        dataType:"html"  		
    });

    return false;
}

function confirmPopUp(command, title, text, yes, no){
	var html = "<div class='fbox_header'><h4>"+title+"</h4></div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<div class='btn-group'>" +
			"<a class=\"btn btn-success btn-sm\" href='javascript:void(0)' onclick='"+command+"'>"+yes+"</a>" +
			"<a class=\"btn btn-default btn-sm\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+no+"</a>" +
			"</div>" +
			"</div></div>";
	jQuery.facebox(html);
}

function confirmDirectPopUp(command, title, text, yes, no){
	var html = "<div class='fbox_header'><h4>"+title+"</h4></div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<div class='btn-group'>" +
			"<a class=\"btn btn-success btn-sm\" href='"+command+"' onclick='popUpLoader()'>"+yes+"</a>" +
			"<a class=\"btn btn-default btn-sm\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+no+"</a>" +
			"</div>" +
			"</div></div>";
	jQuery.facebox(html);
}

function freeze(title, text){
	var html = "<div class='fbox_header'><h4>"+title+"</h4></div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<a class=\"uibutton disable\" href='javascript:void(0)' >Please Wait...</a>" +
			"</div></div>";
	jQuery.facebox(html);
}

function alertPopUp(title, text, ok){
	var html = "<div class='fbox_header'><h4>"+title+"</h4></div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<a class=\"btn btn-success btn-sm\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+ok+"</a>" +
			"</div></div>";
	jQuery.facebox(html);
}

function alertDirectPopUp(title, text, ok, action){
	var html = "<div class='fbox_header'><h4>"+title+"</h4></div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<a class=\"btn btn-success btn-sm\" href='"+action+"' onclick='popUpLoader()'>"+ok+"</a>" +
			"</div></div>";
	jQuery.facebox(html);
}

function popUpLoader(){
	jQuery.facebox('<div style=""><div class="loader">Loading...</div></div>');
}

function maintenance(){
	alertPopUp('Notice', 'I\'m Sorry :(.. This feature is unavailable at the moment..<br />It\'s under construction<br />Comeback later<br /><br />Thank You', 'Close');
}

function alertError(opt){
	opt = typeof opt !== 'undefined' ? opt : 'Tidak diketahui';
	   
	alertPopUp('Kesalahan..', 'Telah terjadi suatu kesalahan.. Silahkan ulangi beberapa saat lagi..<br />Atau hubungi administrator<br /><br />Kode Kesalahan => '+opt, 'Tutup');
}

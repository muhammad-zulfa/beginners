var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management (Laravel Default)
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.less('app.less');
});

/*
 |--------------------------------------------------------------------------
 | Gulp Custom Global Config (Added by EP)
 |--------------------------------------------------------------------------
 */
var gulp = require('gulp'),
	minifyCSS = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	notify = require('gulp-notify'),
	phpunit = require('gulp-phpunit');

var paths = {
		front: {
			css: {
				src: ['./public/template/front/css/*.css', '!./public/template/front/css/*.min.css'],
				dest: './public/template/front/css/'
			}
		},
		backoffice: {
			css: {
				src: ['./public/template/backoffice/stylesheets/*.css', '!./public/template/backoffice/stylesheets/*.min.css'],
				dest: './public/template/backoffice/stylesheets/'
			}
		} 
	};


//CSS
gulp.task('css-front', function() {
	return gulp.src(paths.backoffice.css.src)
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.backoffice.css.dest))
});

gulp.task('css-backoffice', function() {
	return gulp.src(paths.front.css.src)
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.front.css.dest))
});

gulp.task('css', ['css-backoffice', 'css-front']);

// JS
/*
gulp.task('js', function(){  
  return gulp.src([
      paths.dev.vendor+'jquery/dist/jquery.js',
      paths.dev.vendor+'bootstrap/dist/js/bootstrap.js',
      paths.dev.js+'js'
    ])
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.production.js));
});
*/

// PHP Unit
/*
gulp.task('phpunit', function() {  
  var options = {debug: false, notify: true};
  return gulp.src('./tests/*.php')
    .pipe(phpunit('./vendor/bin/phpunit', options))

    .on('error', notify.onError({
      title: 'PHPUnit Failed',
      message: 'One or more tests failed.'
    }))

    .pipe(notify({
      title: 'PHPUnit Passed',
      message: 'All tests passed!'
    }));
});
*/

/**
 * To make things work automatically let's tell Gulp to watch our directories for changes, 
 * and if a file is updated, run the appropriate task.
 */

gulp.task('watch-script', function() {  
	 gulp.watch(paths.front.css.src, ['css-front']);
	 gulp.watch(paths.front.css.src, ['css-backoffice']);
	 //gulp.watch('./tests/*.php', ['phpunit']);
});

/**
 * Original: gulp.task('default', ['css', 'js', 'phpunit', 'watch-script']);
 */
gulp.task('default', ['css']);  

